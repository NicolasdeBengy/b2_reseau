# B1 Réseau 2019 - TP1
# TP1 - Mise en jambes
___
# I. Exploration locale en solo
## 1. Affichage d'informations sur la pile TCP/IP locale

### En ligne de commande

>Carte Wifi
```
ipconfig /all

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
   Adresse physique . . . . . . . . . . . : 40-EC-99-8D-22-23
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::98b1:5cd:8574:8479%27(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.1.36(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : lundi 13 septembre 2021 08:51:55
   Bail expirant. . . . . . . . . . . . . : lundi 13 septembre 2021 12:54:00
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   Serveur DHCP . . . . . . . . . . . . . : 10.33.3.254
   IAID DHCPv6 . . . . . . . . . . . : 138472601
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-26-71-D5-E8-24-4B-FE-6E-79-66
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       10.33.10.148
                                       10.33.10.155
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
``` 
nom: __Carte réseau sans fil Wi-Fi__
MAC:__40-EC-99-8D-22-23__
IP:__10.33.1.36/22__

>Carte Ethernet

```
ipconfig /all

Carte Ethernet Ethernet :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . : home
   Description. . . . . . . . . . . . . . : Realtek PCIe GbE Family Controller
   Adresse physique . . . . . . . . . . . : 24-4B-FE-6E-79-66
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
```

nom: __Carte Ethernet Ethernet__
MAC: __24-4B-FE-6E-79-66__
IP: _aucune_

> Gateway

```
ipconfig /all

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
   Adresse physique . . . . . . . . . . . : 40-EC-99-8D-22-23
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::98b1:5cd:8574:8479%27(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.1.36(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : lundi 13 septembre 2021 08:51:55
   Bail expirant. . . . . . . . . . . . . : lundi 13 septembre 2021 12:54:00
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   Serveur DHCP . . . . . . . . . . . . . : 10.33.3.254
   IAID DHCPv6 . . . . . . . . . . . : 138472601
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-26-71-D5-E8-24-4B-FE-6E-79-66
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       10.33.10.148
                                       10.33.10.155
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```

IP de la gateway: __10.33.3.253/22__

### En graphique (GUI : Graphical User Interface)

> Wifi

_Panneau de configuration\Réseau et Internet\Connexions réseau\Wifi\Détails_

![](https://i.imgur.com/zNdBLys.png)


IP:__10.33.1.36/22__
MAC:__40-EC-99-8D-22-23__
Gateway:__10.33.3.253/22__

* à quoi sert la gateway dans le réseau d'YNOV ?

La gateway permet aux machines du réseau d'YNOV de communiquer avec un autre réseau.

## 2. Modifications des informations
### A. Modification d'adresse IP (part 1)

* Utilisez l'interface graphique de votre OS pour changer d'adresse IP: 
_Panneau de configuration\Réseau et Internet\Connexions réseau\Wifi\Propriétés\Protocole Internet version 4\Utiliser l'adresse IP suivante_

![](https://i.imgur.com/hZLbRIP.png)


* Expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.
Il est possible de perdre son accès internet si on s'attribue une IP qui est déjà utilisée par une autre machine.

### B. Table ARP

> Exploration de la table ARP

```
arp -a

Interface : 192.168.11.1 --- 0xf
  Adresse Internet      Adresse physique      Type
  192.168.11.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.233.1 --- 0x10
  Adresse Internet      Adresse physique      Type
  192.168.233.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.33.1.36 --- 0x1b
  Adresse Internet      Adresse physique      Type
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.232.1 --- 0x1e
  Adresse Internet      Adresse physique      Type
  192.168.232.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```

L'adresse MAC de la passerelle de mon réseau est 00-12-00-40-4c-bf car dans l'interface correspondant à mon ip, à l'IP de la Gateway, l'adresse physique est celle-ci.

> Et si on remplissait un peu la table ?


```
arp -a

Interface : 192.168.11.1 --- 0xf
  Adresse Internet      Adresse physique      Type
  192.168.11.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.233.1 --- 0x10
  Adresse Internet      Adresse physique      Type
  192.168.233.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.33.1.36 --- 0x1b
  Adresse Internet      Adresse physique      Type
🏓10.33.1.103           18-65-90-ce-f5-63     dynamique
🏓10.33.2.8             a4-5e-60-ed-0b-27     dynamique
🏓10.33.1.76            e0-cc-f8-7a-4f-4a     dynamique
  10.33.2.62            b0-7d-64-b1-98-d3     dynamique
  10.33.2.208           a4-b1-c1-72-13-98     dynamique
  10.33.3.33            c8-58-c0-63-5a-92     dynamique
  10.33.3.81            3c-58-c2-9d-98-38     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.232.1 --- 0x1e
  Adresse Internet      Adresse physique      Type
  192.168.232.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```
IP Ping = 🏓


Adresses MAC associées aux adresses IP ping: 
_10.33.1.103: 18-65-90-ce-f5-63_
_10.33.2.8: a4-5e-60-ed-0b-27_
_10.33.1.76: e0-cc-f8-7a-4f-4a_


### C. nmap

* Utilisez nmap pour scanner le réseau de votre carte WiFi et trouver une adresse IP libre

```
nmap -v -sP 10.33.0.0/22
Warning: The -sP option is deprecated. Please use -sn
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 09:17 Paris, Madrid (heure dÆÚtÚ)
Initiating ARP Ping Scan at 09:17
Scanning 1023 hosts [1 port/host]
Completed ARP Ping Scan at 09:18, 33.60s elapsed (1023 total hosts)
Initiating Parallel DNS resolution of 80 hosts. at 09:18
Completed Parallel DNS resolution of 80 hosts. at 09:18, 0.04s elapsed
Nmap scan report for 10.33.0.0 [host down]
Nmap scan report for 10.33.0.1 [host down]
Nmap scan report for 10.33.0.2 [host down]
Nmap scan report for 10.33.0.3 [host down] 
[...]
Initiating Parallel DNS resolution of 1 host. at 09:18
Completed Parallel DNS resolution of 1 host. at 09:18, 0.04s elapsed
Nmap scan report for 10.33.1.36
Host is up.
Read data files from: C:\Program Files (x86)\Nmap
Nmap done: 1024 IP addresses (81 hosts up) scanned in 35.85 seconds
           Raw packets sent: 1996 (55.888KB) | Rcvd: 108 (3.024KB)
```

L'IP 10.33.0.3/22 est libre.

> affichez votre table ARP

```
arp -a

Interface : 192.168.11.1 --- 0xf
  Adresse Internet      Adresse physique      Type
  192.168.11.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.233.1 --- 0x10
  Adresse Internet      Adresse physique      Type
  192.168.233.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.33.1.36 --- 0x1b
  Adresse Internet      Adresse physique      Type
  10.33.0.1             3c-58-c2-9d-98-38     dynamique
  10.33.0.6             84-5c-f3-80-32-07     dynamique
  10.33.0.7             9c-bc-f0-b6-1b-ed     dynamique
  10.33.0.43            2c-8d-b1-94-38-bf     dynamique
  10.33.0.60            e2-ee-36-a5-0b-8a     dynamique
  10.33.0.96            ca-4f-f4-af-8f-0c     dynamique
  10.33.0.100           e8-6f-38-6a-b6-ef     dynamique
  10.33.0.119           18-56-80-70-9c-48     dynamique
  10.33.0.140           40-ec-99-8b-11-c2     dynamique
  10.33.0.148           e6-aa-26-ee-23-b7     dynamique
  10.33.0.180           26-91-29-98-e2-9d     dynamique
  10.33.0.242           74-4c-a1-51-1e-61     dynamique
  10.33.1.70            30-57-14-94-de-fb     dynamique
  10.33.1.166           1a-41-0b-54-a5-a0     dynamique
  10.33.1.194           20-16-b9-84-86-1d     dynamique
  10.33.1.224           d0-c6-37-3c-59-a1     dynamique
  10.33.1.238           50-76-af-88-6c-0b     dynamique
  10.33.1.239           ec-2e-98-ca-95-97     dynamique
  10.33.1.242           34-7d-f6-5a-20-da     dynamique
  10.33.1.243           34-7d-f6-5a-20-da     dynamique
  10.33.1.248           e8-84-a5-24-94-c9     dynamique
  10.33.2.24            02-24-5e-d2-c9-1e     dynamique
  10.33.2.51            a8-64-f1-37-d1-15     dynamique
  10.33.2.88            68-3e-26-7c-c3-1a     dynamique
  10.33.2.105           ec-2e-98-ca-da-e9     dynamique
  10.33.2.151           10-32-7e-38-50-c3     dynamique
  10.33.2.173           34-2e-b7-47-f9-28     dynamique
  10.33.2.182           f4-4e-e3-c0-ed-29     dynamique
  10.33.2.188           d8-f3-bc-c2-f5-39     dynamique
  10.33.2.196           08-71-90-87-b9-8c     dynamique
  10.33.2.201           d8-3b-bf-98-b1-6f     dynamique
  10.33.2.241           9c-6b-72-9b-aa-8b     dynamique
  10.33.3.59            02-47-cd-3d-d4-e9     dynamique
  10.33.3.74            40-ec-99-f0-81-b0     dynamique
  10.33.3.80            3c-58-c2-9d-98-38     dynamique
  10.33.3.89            f0-9e-4a-52-94-f0     dynamique
  10.33.3.105           54-14-f3-b5-aa-36     dynamique
  10.33.3.139           34-cf-f6-37-2c-fb     dynamique
  10.33.3.168           12-88-c1-71-b7-a0     dynamique
  10.33.3.187           96-fd-87-13-4b-ee     dynamique
  10.33.3.218           8c-85-90-65-6a-52     dynamique
  10.33.3.248           34-7d-f6-5a-89-99     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.254           00-0e-c4-cd-74-f5     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.232.1 --- 0x1e
  Adresse Internet      Adresse physique      Type
  192.168.232.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.156.1 --- 0x4c
  Adresse Internet      Adresse physique      Type
  192.168.156.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.58.1 --- 0x51
  Adresse Internet      Adresse physique      Type
  192.168.58.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.56.1 --- 0x56
  Adresse Internet      Adresse physique      Type
  192.168.56.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.12.1 --- 0x5b
  Adresse Internet      Adresse physique      Type
  192.168.12.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.10.1.1 --- 0x60
  Adresse Internet      Adresse physique      Type
  10.10.1.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```

### D. Modification d'adresse IP (part 2)

* Modifiez de nouveau votre adresse IP vers une adresse IP que vous savez libre grâce à nmap

> utilisez un ping scan sur le réseau YNOV
```
nmap -v -sP 10.33.0.0/22
Warning: The -sP option is deprecated. Please use -sn
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 09:17 Paris, Madrid (heure dÆÚtÚ)
Initiating ARP Ping Scan at 09:17
Scanning 1023 hosts [1 port/host]
Completed ARP Ping Scan at 09:18, 33.60s elapsed (1023 total hosts)
Initiating Parallel DNS resolution of 80 hosts. at 09:18
Completed Parallel DNS resolution of 80 hosts. at 09:18, 0.04s elapsed
Nmap scan report for 10.33.0.0 [host down]
Nmap scan report for 10.33.0.1 [host down]
Nmap scan report for 10.33.0.2 [host down]
Nmap scan report for 10.33.0.3 [host down] 
[...]
Initiating Parallel DNS resolution of 1 host. at 09:18
Completed Parallel DNS resolution of 1 host. at 09:18, 0.04s elapsed
Nmap scan report for 10.33.1.36
Host is up.
Read data files from: C:\Program Files (x86)\Nmap
Nmap done: 1024 IP addresses (81 hosts up) scanned in 35.85 seconds
           Raw packets sent: 1996 (55.888KB) | Rcvd: 108 (3.024KB)
```

```
ipconfig /all
[...]
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
   Adresse physique . . . . . . . . . . . : 40-EC-99-8D-22-23
   DHCP activé. . . . . . . . . . . . . . : Non ⚪
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::98b1:5cd:8574:8479%27(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.0.3(préféré) ⚪
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253 🔴
   IAID DHCPv6 . . . . . . . . . . . : 138472601
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-26-71-D5-E8-24-4B-FE-6E-79-66
   Serveurs DNS. . .  . . . . . . . . . . : fec0:0:0:ffff::1%1
                                       fec0:0:0:ffff::2%1
                                       fec0:0:0:ffff::3%1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
[...]
```
IP choisi manuellement: ⚪
Passerelle bien définie: 🔴
Accès internet:
```
ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=17 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=18 ms TTL=115

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 2, reçus = 2, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 17ms, Maximum = 18ms, Moyenne = 17ms
```

# II. Exploration locale en duo

## 3. Modification d'adresse IP

Configuration des machines :
Le premier aura 192.168.0.1 et le 2ème 192.168.0.2.
![](https://i.imgur.com/ods44w3.png)

Les changements ont bien pris effet :
```
PS C:\Users\DIRECTEUR_PC2> ipconfig

Configuration IP de Windows
[...]
Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::f166:4347:db54:1052%10
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.0.1
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . :
[...]
```

Ping de la 2ème machine :
```
PS C:\Users\DIRECTEUR_PC2> ping 192.168.0.2

Envoi d’une requête 'Ping'  192.168.0.2 avec 32 octets de données :
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 192.168.0.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 2ms, Moyenne = 1ms
```
## 4. Utilisation d'un des deux comme gateway

Depuis la 2ème machine :

Ping 8.8.8.8 ->
```
PS C:\Users\nicol> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=25 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=34 ms TTL=114

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 2, reçus = 2, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 25ms, Maximum = 34ms, Moyenne = 29ms
```

tracert vers www.google.com ->
```
PS C:\Users\nicol> tracert www.google.com

Détermination de l’itinéraire vers www.google.com [142.250.179.68]
avec un maximum de 30 sauts :

  1     3 ms     2 ms     2 ms  DIRECTEUR-PC2 [192.168.0.1]
  2     *        *        *     Délai d’attente de la demande dépassé.
  3     4 ms     5 ms     4 ms  10.33.3.253
  4     5 ms     6 ms     7 ms  10.33.10.254
  5     4 ms     3 ms     4 ms  reverse.completel.net [92.103.174.137]
  6    11 ms     9 ms     8 ms  92.103.120.182
  7    21 ms    19 ms    21 ms  172.19.130.113
  8    19 ms    19 ms    19 ms  46.218.128.78
  9    21 ms    21 ms    21 ms  186.144.6.194.rev.sfr.net [194.6.144.186]
 10    22 ms    21 ms    22 ms  186.144.6.194.rev.sfr.net [194.6.144.186]
 11    22 ms    22 ms    23 ms  72.14.194.30
 12    23 ms    22 ms    22 ms  108.170.231.111
 13    22 ms    21 ms    21 ms  142.251.49.133
 14    21 ms    21 ms    21 ms  par21s19-in-f4.1e100.net [142.250.179.68]

Itinéraire déterminé.
```

## 5. Petit chat privé

Serveur :
```
PS C:\Users\DIRECTEUR_PC2\Desktop\Ynov> .\nc.exe -l -p 8888
Salut !
Hola ! Ca va ?
Super et toi ?
Je sais pas mettre de GIF dans ce truc... Donc ca va pas !
gifdechat.gif
=(
```

Client :
```
PS C:\Users\nicol\Downloads> .\nc.exe 192.168.0.1 8888
Salut !
[...]
```
Si on veut préciser sur quelle ip écouter (en mode serveur) : `.\nc.exe -l -p 8888 192.168.0.2`

## 6. Firewall

Pour activer les ping, il faut autoriser les règles `File and Printer Sharing (Echo Request - ICMPv4-In)` dans le pare feu.
Pour NETCAT, il faut ajouter une règle avec le programme et ajouter les ports voulus.

![](https://i.imgur.com/xy1TLTH.png)

![](https://i.imgur.com/A8WW3Kt.png)

# III. Manipulations d'autres outils/protocoles côté client

## 1. DHCP

```
ipconfig /all
[...]
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
   Adresse physique . . . . . . . . . . . : 40-EC-99-8D-22-23
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::98b1:5cd:8574:8479%27(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.1.36(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : jeudi 16 septembre 2021 11:48:34
   Bail expirant. . . . . . . . . . . . . : jeudi 16 septembre 2021 13:48:33🟥
   Adresse d’autoconfiguration IPv4 . . . : 169.254.132.121(tentative)
   Masque de sous-réseau. . . . . . . . . : 255.255.0.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   Serveur DHCP . . . . . . . . . . . . . : 10.33.3.254 ⬜
   IAID DHCPv6 . . . . . . . . . . . : 138472601
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-26-71-D5-E8-24-4B-FE-6E-79-66
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       10.33.10.148
                                       10.33.10.155
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
[...]
```
IP du serveur DHCP: ⬜
Date d'expiration du bail DHCP: 🟥

## 2. DNS

* trouver l'adresse IP du serveur DNS que connaît votre ordinateur

```
ipconfig /all
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
   Adresse physique . . . . . . . . . . . : 40-EC-99-8D-22-23
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::98b1:5cd:8574:8479%27(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.1.36(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : jeudi 16 septembre 2021 11:48:33
   Bail expirant. . . . . . . . . . . . . : jeudi 16 septembre 2021 13:48:33
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   Serveur DHCP . . . . . . . . . . . . . : 10.33.3.254
   IAID DHCPv6 . . . . . . . . . . . : 138472601
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-26-71-D5-E8-24-4B-FE-6E-79-66
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2🔷
                                            10.33.10.148🔷
                                            10.33.10.155🔷
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```
DNS: 🔷

* nslookup

```
nslookup google.com
Serveur :   UnKnown
Address:  10.33.10.2

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:80e::200e
          142.250.179.78
```
Le serveur DNS a comme IP 10.33.10.2, le nom domaine est google.com et son IP correspondante est 142.250.179.78
```
nslookup ynov.com
Serveur :   UnKnown
Address:  10.33.10.2

Réponse ne faisant pas autorité :
Nom :    ynov.com
Address:  92.243.16.143
```
Le serveur DNS a comme IP 10.33.10.2, le nom domaine est ynov.com et son IP correspondante est 92.243.16.143


L'adresse IP du serveur à qui je viens d'effectuer ces requêtes est 10.33.10.2

```
nslookup 78.74.21.21
Serveur :   UnKnown
Address:  10.33.10.2

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21
```
Le serveur DNS a comme IP 10.33.10.2, le nom domaine est host-78-74-21-21.homerun.telia.com et son IP correspondante est 78.74.21.21
```
nslookup 92.146.54.88
Serveur :   UnKnown
Address:  10.33.10.2

Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
Address:  92.146.54.88
```
Le serveur DNS a comme IP 10.33.10.2, le nom domaine est apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr et son IP correspondante est 92.146.54.88

# IV. Wireshark

* un ping entre vous et la passerelle
![](https://i.imgur.com/bROrZSv.png)
La source et la destination sont mon IP et celle de la Gateway et le protocole est ICMP

* un netcat entre vous et votre mate, branché en RJ45
![](https://i.imgur.com/vSHjbTQ.png)
La source et la destination sont mon IP et celle de mon mate et le protocole est TCP

* une requête DNS.
![](https://i.imgur.com/tUtTjES.png)
La source et la destination sont mon IP et celle du serveur DNS et le protocole est DNS
