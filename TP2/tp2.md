# TP2 : On va router des trucs
# I. ARP
## 1. Echange ARP

### 🌞Générer des requêtes ARP

* effectuer un ping d'une machine à l'autre

__node1:__
```
[nioa@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.600 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.475 ms

--- 10.2.1.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1044ms
rtt min/avg/max/mdev = 0.475/0.537/0.600/0.066 ms
```

__node2:__
```
[nioa@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.347 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.445 ms

--- 10.2.1.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1064ms
rtt min/avg/max/mdev = 0.347/0.396/0.445/0.049 ms
```

* observer les tables ARP des deux machin

__node1:__
```
[nioa@node1 ~]$ ip n s
10.2.1.12 dev enp0s8 lladdr ⏩08:00:27:23:24:08⏪ STALE
10.0.2.2 dev enp0s3 lladdr    52:54:00:12:35:02    STALE
10.2.1.1 dev enp0s8 lladdr    0a:00:27:00:00:66    REACHABLE
```
Adresse MAC de node2: ⏩[...]⏪

__node2:__
```
[nioa@node2 ~]$ ip n s
10.2.1.11 dev enp0s8 lladdr ⏩08:00:27:db:af:ee⏪ STALE
10.0.2.2 dev enp0s3 lladdr    52:54:00:12:35:02    STALE
10.2.1.1 dev enp0s8 lladdr    0a:00:27:00:00:66    DELAY
```
Adresse MAC de node1: ⏩[...]⏪

* prouvez que l'info est correcte (que l'adresse MAC que vous voyez dans la table est bien celle de la machine correspondante)

__Table ARP de node1:__
```
[nioa@node1 ~]$ ip n s
10.2.1.12 dev enp0s8 lladdr 08:00:27:23:24:08 STALE
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:66 REACHABLE
```

__MAC de node2: ⏩[...]⏪__
```
[nioa@node2 ~]$ ip a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether ⏩08:00:27:23:24:08⏪ brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe23:2408/64 scope link
       valid_lft forever preferred_lft forever
```

## 2. Analyse de trames

### Analyse de trames

| ordre | type trame  | source                   | destination                |
|-------|-------------|--------------------------|----------------------------|
| 1     | Requête ARP | `node1` `08:00:27:23:24:08` | Broadcast `ff:ff:ff:ff:ff:ff` |
| 2     | Réponse ARP | `node2` `08:00:27:db:af:ee` | `node1` `08:00:27:23:24:08`   |

# II. Routage

## 1. Mise en place du routage

* Activer le routage sur le noeud router.2.tp2

```
[nioa@router ~]$ sudo firewall-cmd --list-all
[sudo] password for nioa:
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[nioa@router ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s3 enp0s9 enp0s8
[nioa@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[nioa@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```
* Ajouter les routes statiques nécessaires pour que node1.net1.tp2 et marcel.net2.tp2 puissent se ping

**node1 :**
```
[nioa@node1 ~]$ cat /etc/sysconfig/network-scripts/route-enp0s3
10.2.2.0/24 via 10.2.1.11 dev enp0s3
[nioa@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=0.479 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=0.401 ms

--- 10.2.2.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.401/0.440/0.479/0.039 ms
```

**marcel :**

```
[nioa@marcel ~]$ cat /etc/sysconfig/network-scripts/route-enp0s3
10.2.1.0/24 via 10.2.2.11 dev enp0s3
[nioa@marcel ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=63 time=0.813 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=63 time=0.934 ms

--- 10.2.1.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1009ms
rtt min/avg/max/mdev = 0.813/0.873/0.934/0.067 ms
```


## 2. Analyse de trames

* Analyse des échanges ARP

> videz les tables ARP des trois noeuds

Sur les trois machines :
```
[nioa@marcel ~]$ sudo ip n flush all
```

> effectuez un ping de node1.net1.tp2 vers marcel.net2.tp2

```
[nioa@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.17 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=0.943 ms

--- 10.2.2.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 0.943/1.055/1.167/0.112 ms
```

> regardez les tables ARP des trois noeuds

**node1 :**
```
[nioa@node1 ~]$ ip n s
10.2.1.1 dev enp0s3 lladdr 0a:00:27:00:00:66 DELAY
10.2.1.11 dev enp0s3 lladdr 08:00:27:2e:b0:e9 STALE
```
**router :**
```
[nioa@router ~]$ ip n s
10.2.2.12 dev enp0s9 lladdr 08:00:27:a6:5a:f8 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:66 DELAY
10.2.1.12 dev enp0s8 lladdr 08:00:27:69:9b:34 STALE
```
**marcel :**
```
[nioa@marcel ~]$ ip n s
10.2.2.11 dev enp0s3 lladdr 08:00:27:f7:d1:51 STALE
10.2.2.1 dev enp0s3 lladdr 0a:00:27:00:00:6b DELAY
```

> essayez de déduire un peu les échanges ARP qui ont eu lieu

'node1' fait une requête ARP en broadcast pour trouver 'router', 'router' lui répond en unicast, 'router' fait une requête ARP en broadcast pour trouver 'marcel' et 'marcel' lui répond en unicast. 

> écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, puis le ping et le pong, je veux TOUTES les trames utiles pour l'échange

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP | x         | `node1` `08:00:27:69:9b:34` | x            | Broadcast `ff:ff:ff:ff:ff` |
| 2     | Réponse ARP | x         | `router` `08:00:27:69:9b:34` | x           | `node1` `08:00:27:2e:b0:e9` |
| 3     | Requête ARP | x         | `router` `08:00:27:69:9b:34` | x           | Broadcast `ff:ff:ff:ff:ff` |
| 4     | Réponse ARP | x         | `marcel` `08:00:27:a6:5a:f8` | x           | `router` `08:00:27:69:9b:34` |
| 5     | Ping        | 10.2.1.12 | `node1` `08:00:27:69:9b:34` | 10.2.2.12    | `marcel` `08:00:27:a6:5a:f8` |
| 6     | Pong        | 10.2.2.12 | `marcel` `08:00:27:a6:5a:f8` | 10.2.1.12 | `node1` `08:00:27:69:9b:34` |

## 3. Accès internet

* Donnez un accès internet à vos machines

> le routeur a déjà un accès internet

```
[nioa@router ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=23.6 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=23.8 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 23.643/23.714/23.786/0.169 ms
[nioa@router ~]$ ping google.com
PING google.com (142.250.178.142) 56(84) bytes of data.
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=1 ttl=114 time=23.3 ms
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=2 ttl=114 time=24.8 ms

--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 23.283/24.064/24.846/0.796 ms
```

> ajoutez une route par défaut à node1.net1.tp2 et marcel.net2.tp2

**node1 :**
```
[nioa@node1 ~]$ ip r s
default via 10.2.1.11 dev enp0s3 proto static metric 100
[...]
```
**marcel :**
```
[nioa@marcel ~]$ ip r s
default via 10.2.2.11 dev enp0s3 proto static metric 100
[...]
```

>> vérifiez que vous avez accès internet avec un ping

**node1 :**
```
[nioa@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=118 time=31.1 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=118 time=10.8 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 10.796/20.956/31.117/10.161 ms
```
**marcel :**
```
[nioa@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=118 time=14.6 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=118 time=31.7 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 14.643/23.182/31.721/8.539 ms
```

> donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser

**node1 & marcel :**
```
[nioa@node1 ~]$ sudo cat /etc/resolv.conf
nameserver 1.1.1.1
```

>> vérifiez que vous avez une résolution de noms qui fonctionne avec dig

**node1 :**
```
[nioa@node1 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 64906
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               10800   IN      A       92.243.16.143

;; Query time: 14 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Fri Sep 24 17:33:42 CEST 2021
;; MSG SIZE  rcvd: 53
```

**marcel :**
```
[nioa@marcel ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 21435
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               10782   IN      A       92.243.16.143

;; Query time: 12 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Fri Sep 24 17:33:59 CEST 2021
;; MSG SIZE  rcvd: 53
```
>> puis avec un ping vers un nom de domaine

**node1 :**
```
[nioa@node1 ~]$ ping ynov.com
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=55 time=9.94 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=55 time=10.1 ms

--- ynov.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 9.944/10.027/10.111/0.130 ms
```

**marcel :**
```
[nioa@marcel ~]$ ping ynov.com
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=55 time=10.9 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=55 time=11.2 ms

--- ynov.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 10.865/11.008/11.152/0.177 ms
```
* Analyse de trames

| ordre | type trame | IP source           | MAC source               | IP destination | MAC destination |
|-------|------------|---------------------|--------------------------|----------------|-----------------|
| 1     | ping       | `node1` `10.2.1.12` | `node1` `08:00:27:69:9b:34` | `8.8.8.8` | `08:00:27:2e:b0:e9` |
| 2     | pong       | `8.8.8.8` | `08:00:27:2e:b0:e9` | `node1` `10.2.1.12` | `node1` `08:00:27:69:9b:34` |

# III. DHCP
## 1. Mise en place du serveur DHCP

* Sur la machine node1.net1.tp2, vous installerez et configurerez un serveur DHCP (go Google "rocky linux dhcp server").

**Fichier de conf du server DHCP: **
```
[nioa@node1 ~]$ sudo cat /etc/dhcp/dhcpd.conf
[sudo] password for nioa:
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 600;
max-lease-time 7200;
authoritative;
# specify network address and subnetmask
subnet 10.2.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.2.1.50 10.2.1.254;
    # specify broadcast address
    option broadcast-address 10.2.1.255;
    # specify gateway
    option routers 10.2.1.11;
}
```
Puis on autaurise le dhcp sur le firewall:
```
[nioa@node1 ~]$ sudo firewall-cmd --add-service=dhcp
success
[nioa@node1 ~]$ sudo firewall-cmd --runtime-to-permanent
success
```

> faites lui récupérer une IP en DHCP à l'aide de votre serveur

```
[nioa@node2 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=dhcp
ONBOOT=yes
[nioa@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:2a:49:f2 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.50/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 350sec preferred_lft 350sec
    inet6 fe80::a00:27ff:fe2a:49f2/64 scope link
       valid_lft forever preferred_lft forever
```

* Améliorer la configuration du DHCP

> ajoutez de la configuration à votre DHCP pour qu'il donne aux clients, en plus de leur IP :

Une route par défaut: ⚪


Un serveur DNS à utiliser: 🔴

```
[nioa@node1 ~]$ sudo cat /etc/dhcp/dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 600;
max-lease-time 7200;
authoritative;
# specify network address and subnetmask
subnet 10.2.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.2.1.50 10.2.1.254;
    # specify broadcast address
    option broadcast-address 10.2.1.255;
    # specify gateway
    option routers 10.2.1.11; ⚪
    option domain-name-servers 1.1.1.1; 🔴
}
```
> récupérez de nouveau une IP en DHCP sur node2.net1.tp2 pour tester :

Il a récupéré son IP: 🟡
Il peut ping sa passerelle: 🟢

```
[nioa@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:2a:49:f2 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.50/24🟡 brd 10.2.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 584sec preferred_lft 584sec
    inet6 fe80::a00:27ff:fe2a:49f2/64 scope link
       valid_lft forever preferred_lft forever
[nioa@node2 ~]$ ping 10.2.1.11 🟢
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.280 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.335 ms

--- 10.2.1.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1009ms
rtt min/avg/max/mdev = 0.280/0.307/0.335/0.032 ms
```

> il doit avoir une route par défaut

Présence de la route: 🔵
Ping vers une IP: 🟣
```
[nioa@node2 ~]$ ip r s
🔵default via 10.2.1.11 dev enp0s3 proto dhcp metric 100
10.2.1.0/24 dev enp0s3 proto kernel scope link src 10.2.1.50 metric 100
[nioa@node2 ~]$ ping 1.2.3.4 🟣
PING 1.2.3.4 (1.2.3.4) 56(84) bytes of data.

--- 1.2.3.4 ping statistics ---
3 packets transmitted, 0 received, 100% packet loss, time 2067ms
```

> il doit connaître l'adresse d'un serveur DNS pour avoir de la résolution de noms

La commande dig que ça fonctionne: 🟤
Ping vers un nom de domaine: ⚫
```
[nioa@node2 ~]$ dig ynov.com 🟤

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 62562
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               9368    IN      A       92.243.16.143

;; Query time: 14 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sat Sep 25 21:52:00 CEST 2021
;; MSG SIZE  rcvd: 53

[nioa@node2 ~]$ ping ynov.com ⚫
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=49 time=12.6 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=49 time=11.1 ms

--- ynov.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 11.146/11.859/12.572/0.713 ms
```

## 2. Analyse de trames
### Analyse de trames

| ordre | type trame  | MAC source                              | MAC destination            | IP Source      | IP Destination |
|-------|-------------|-----------------------------------------|----------------------------|----------------|----------------|
| 1     | Requête ARP | `node2` `08:00:27:2a:49:f2`             | Broadcast `ff:ff:ff:ff:ff` | x | x |
| 2     | Réponse ARP | `node1/dhcp server` `08:00:27:69:9b:34` | `node2` `08:00:27:2a:49:f2` | x | x |
| 3     | DHCP (Discover) | `node2` `08:00:27:2a:49:f2`            | Broadcast `ff:ff:ff:ff:ff` | `0.0.0.0` | `255.255.255.255` |
| 4     | Requête ARP | `node1/dhcp server` `08:00:27:69:9b:34` | Broadcast `ff:ff:ff:ff:ff` | x | x |
| 5     | DHCP (Offer) | `node1/dhcp server` `08:00:27:69:9b:34` | `node2` `08:00:27:2a:49:f2` | `10.2.1.12` | `10.2.1.51` |
| 6     | DHCP (Request) | `node2` `08:00:27:2a:49:f2`            | Broadcast `ff:ff:ff:ff:ff` | `0.0.0.0` | `255.255.255.255` |
| 7     | DHCP (Acknowledge) | `node1/dhcp server` `08:00:27:69:9b:34` | `node2` `08:00:27:2a:49:f2` | `10.2.1.12` | `10.2.1.51` |
| 8     | Requête ARP | `node2` `08:00:27:2a:49:f2`             | Broadcast `ff:ff:ff:ff:ff` | x | x |
| 9     | Requête ARP | `node1/dhcp server` `08:00:27:69:9b:34` | Broadcast `ff:ff:ff:ff:ff` | x | x |

L'échange DORA correspond aux trames 3, 5, 6 et 7
