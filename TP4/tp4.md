# TP4 : Vers un réseau d'entreprise
# I. Dumb switch
## 3. Setup topologie 1
### Commençons simple
```bash
# définissez les IPs statiques sur les deux VPCS

# PC1
PC1> ip 10.1.1.1/24
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0

PC1> show ip

NAME        : PC1[1]
IP/MASK     : 10.1.1.1/24
GATEWAY     : 0.0.0.0
DNS         :
MAC         : 00:50:79:66:68:00
LPORT       : 20038
RHOST:PORT  : 127.0.0.1:20039
MTU         : 1500

#PC2
PC2> ip 10.1.1.2/24
Checking for duplicate address...
PC1 : 10.1.1.2 255.255.255.0

PC2> show ip

NAME        : PC2[1]
IP/MASK     : 10.1.1.2/24
GATEWAY     : 0.0.0.0
DNS         :
MAC         : 00:50:79:66:68:01
LPORT       : 20004
RHOST:PORT  : 127.0.0.1:20005
MTU         : 1500

# ping un VPCS depuis l'autre
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=16.901 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=4.964 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=3.932 ms
84 bytes from 10.1.1.2 icmp_seq=4 ttl=64 time=7.280 ms
84 bytes from 10.1.1.2 icmp_seq=5 ttl=64 time=4.937 ms
```

# II. VLAN
## 3. Setup topologie 2
### Adressage
```bash
# définissez les IPs statiques sur tous les VPCS
PC1> show ip

NAME        : PC1[1]
IP/MASK     : 10.1.1.1/24
[...]

PC2> show ip

NAME        : PC2[1]
IP/MASK     : 10.1.1.2/24
[...]

PC3> show ip

NAME        : PC3[1]
IP/MASK     : 10.1.1.3/24
[...]

# vérifiez avec des ping que tout le monde se ping
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=19.751 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=5.556 ms

PC1> ping 10.1.1.3

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=8.247 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=7.231 ms

PC2> ping 10.1.1.3

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=8.388 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=4.985 ms
```

### Configuration des VLANs

```bash
# déclaration des VLANs sur le switch sw1

Switch>enable
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)#vlan 10
Switch(config-vlan)#name admins
Switch(config-vlan)#exit
Switch(config)#vlan 20
Switch(config-vlan)#name guests
Switch(config-vlan)#exit
Switch(config)#exit
Switch#show vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
[...]
10   admins                           active
20   guests                           active
[...]

# ajout des ports du switches dans le bon VLAN (voir le tableau d'adressage de la topo 2 juste au dessus)
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)#interface Gi0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10
Switch(config-if)#exit
Switch(config)#interface Gi0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10
Switch(config-if)#exit
Switch(config)#interface Gi0/3
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 20
Switch(config-if)#exit
Switch(config)#exit
Switch#
*Oct 21 07:53:01.538: %SYS-5-CONFIG_I: Configured from console by console
Switch#show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/0, Gi1/0, Gi1/1, Gi1/2
                                                Gi1/3, Gi2/0, Gi2/1, Gi2/2
                                                Gi2/3, Gi3/0, Gi3/1, Gi3/2
                                                Gi3/3
10   admins                           active    Gi0/1, Gi0/2
20   guests                           active    Gi0/3
[...]
```

### Vérif
```bash
# pc1 et pc2 doivent toujours pouvoir se ping
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=8.498 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=3.918 ms

# pc3 ne ping plus personne
PC3> ping 10.1.1.1

host (10.1.1.1) not reachable

PC3> ping 10.1.1.2

host (10.1.1.2) not reachable
```

# III. Routing
## 3. Setup topologie 3
### Adressage
```bash
# définissez les IPs statiques sur toutes les machines sauf le routeur

# PC1
PC1> show ip

NAME        : PC1[1]
IP/MASK     : 10.1.1.1/24
[...]

#PC2
PC2> show ip

NAME        : PC2[1]
IP/MASK     : 10.1.1.2/24
[...]

# adm1
adm1> show ip

NAME        : adm1[1]
IP/MASK     : 10.2.2.1/24

# web1.servers.tp4
[nioa@web1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
BOOTPROTO=static
DEFROUTE=yes
NAME=enp0s3
DEVICE=enp0s3
ONBOOT=yes
IPADDR=10.3.3.1
NETMASK=255.255.255.0
```

### Configuration des VLANs

```bash
# déclaration des VLANs sur le switch sw1
Switch>enable
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)#vlan 11
Switch(config-vlan)#name clients
Switch(config-vlan)#exit
Switch(config)#vlan 12
Switch(config-vlan)#name admins
Switch(config-vlan)#exit
Switch(config)#vlan 13
Switch(config-vlan)#name servers
Switch(config-vlan)#exit

# ajout des ports du switches dans le bon VLAN (voir le tableau d'adressage de la topo 2 juste au dessus)
Switch(config)#interface Gi0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit
Switch(config)#interface Gi0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit
Switch(config)#interface Gi0/3
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 13
Switch(config-if)#exit
Switch(config)#interface Gi1/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 12
Switch(config-if)#exit
Switch(config)#exit
Switch#show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/0, Gi1/0, Gi1/2, Gi1/3
                                                Gi2/0, Gi2/1, Gi2/2, Gi2/3
                                                Gi3/0, Gi3/1, Gi3/2, Gi3/3
10   VLAN0010                         active
11   clients                          active    Gi0/1, Gi0/2
12   admins                           active    Gi1/1
13   servers                          active    Gi0/3
[...]

# il faudra ajouter le port qui pointe vers le routeur comme un trunk : c'est un port entre deux équipements réseau (un switch et un routeur)
Switch#conf t
Switch(config)#interface Gi1/0
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 11,12,13
Switch(config-if)#exit
Switch(config)#exit
Switch#show interface trunk

Port        Mode             Encapsulation  Status        Native vlan
Gi1/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi1/0       1-4094

Port        Vlans allowed and active in management domain
Gi1/0       1,10-13,20

Port        Vlans in spanning tree forwarding state and not pruned
Gi1/0       1,10-13,20
```

### Config du routeur

```bash
# attribuez ses IPs au routeur
R1#conf t
R1(config)#interface FastEthernet 0/0.11
R1(config-subif)#encapsulation dot1Q 11 
R1(config-subif)#ip address 10.1.1.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface FastEthernet 0/0.12
R1(config-subif)#encapsulation dot1Q 12 
R1(config-subif)#ip address 10.2.2.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface FastEthernet 0/0.13
R1(config-subif)#encapsulation dot1Q 13 
R1(config-subif)#ip address 10.3.3.254 255.255.255.0
R1(config-subif)#exit
R1(config)#exit

R1#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES manual up                    up
FastEthernet0/0.11          10.1.1.254      YES manual up                    up
FastEthernet0/0.12          10.2.2.254      YES manual up                    up
FastEthernet0/0.13          10.3.3.254      YES manual up                    up
[...]
```

### Vérif

```bash
# tout le monde doit pouvoir ping le routeur sur l'IP qui est dans son réseau

# PC1
PC1> ping 10.1.1.254
84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=29.892 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=15.895 ms

# PC2
PC2> ping 10.1.1.254
84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=16.509 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=15.564 ms

# adm1
adm1> ping 10.2.2.254
84 bytes from 10.2.2.254 icmp_seq=1 ttl=255 time=23.950 ms
84 bytes from 10.2.2.254 icmp_seq=2 ttl=255 time=15.260 ms
# web
[nioa@web1 ~]$ ping 10.3.3.254
PING 10.3.3.254 (10.3.3.254) 56(84) bytes of data.
64 bytes from 10.3.3.254: icmp_seq=1 ttl=255 time=37.4 ms
64 bytes from 10.3.3.254: icmp_seq=2 ttl=255 time=14.7 ms
[...]

# ajoutez une route par défaut sur les VPCS

# PC1
PC1> ip 10.1.1.1/24 10.1.1.254
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0 gateway 10.1.1.254

# PC2
PC2> ip 10.1.1.2/24 10.1.1.254
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0 gateway 10.1.1.254

# adm1
adm1> ip 10.2.2.1/24 10.2.2.254
Checking for duplicate address...
adm1 : 10.2.2.1 255.255.255.0 gateway 10.2.2.254


# ajoutez une route par défaut sur la machine virtuelle

[nioa@web1 ~]$ sudo vim /etc/sysconfig/network-scripts/ifcfg-enp0s3
[...]
GATEWAY=10.3.3.254

# testez des ping entre les réseaux

# 10.1.1.0/24 vers 10.3.3.0/24
PC1> ping 10.3.3.1
84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=36.064 ms
84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=32.596 ms

# 10.2.2.0/24 vers 10.1.1.0/24
adm1> ping 10.1.1.1
84 bytes from 10.1.1.1 icmp_seq=1 ttl=63 time=19.453 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=63 time=41.687 ms

# 10.3.3.0/24 vers 10.2.2.0/24
[nioa@web1 ~]$ ping 10.2.2.1
PING 10.2.2.1 (10.2.2.1) 56(84) bytes of data.
64 bytes from 10.2.2.1: icmp_seq=1 ttl=63 time=30.7 ms
64 bytes from 10.2.2.1: icmp_seq=2 ttl=63 time=25.0 ms
```

# IV. NAT
## 3. Setup topologie 4
### Ajoutez le noeud Cloud à la topo

```bash
# côté routeur, il faudra récupérer un IP en DHCP
R1#conf t
R1(config)#interface FastEthernet 1/0
R1(config-if)#ip address dhcp
R1(config-if)#no shut
R1(config-if)#exit
R1(config)#exit
R1#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
[...]
FastEthernet1/0            10.0.3.16       YES DHCP   up                    up
[...]

# vous devriez pouvoir ping 1.1.1.1
R1#ping 1.1.1.1
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 1.1.1.1, timeout is 2 seconds:
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 20/22/28 ms
```

### Configurez le NAT

```
R1#conf t
R1(config)#interface FastEthernet 1/0
R1(config-if)#ip nat outside
R1(config-if)#exit
R1(config)#interface FastEthernet 0/0
R1(config-if)#ip nat inside
R1(config-if)#exit
R1(config)#access-list 1 permit any
R1(config)#ip nat inside source list 1 interface FastEthernet 1/0 overload
```

### Test

```bash
# ajoutez une route par défaut (si c'est pas déjà fait) -> Déjà fait dans la partie précédente

# configurez l'utilisation d'un DNS
    # sur les VPCS
PC1/PC2/adm1> ip dns 1.1.1.1

    # sur la machine Linux
[nioa@web1 ~]$ cat /etc/resolv.conf
nameserver 1.1.1.1

# vérifiez un ping vers un nom de domaine
PC1/PC2/adm1> ping google.com
google.com resolved to 142.250.179.110
84 bytes from 142.250.179.110 icmp_seq=1 ttl=118 time=38.447 ms
84 bytes from 142.250.179.110 icmp_seq=2 ttl=118 time=37.453 ms

[nioa@web1 ~]$ ping google.com
PING google.com (142.250.179.110) 56(84) bytes of data.
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=1 ttl=118 time=39.4 ms
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=2 ttl=118 time=32.3 ms
```

# V. Add a building
## 3. Setup topologie 5
### Vous devez me rendre le show running-config de tous les équipements
```bash
# le routeur
R1#show running-config
Building configuration...

Current configuration : 1356 bytes
!
version 12.4
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
!
hostname R1
!
boot-start-marker
boot-end-marker
!
no aaa new-model
memory-size iomem 5
no ip icmp rate-limit unreachable
!
ip cef
no ip domain lookup
!
ip tcp synwait-time 5
!
interface FastEthernet0/0
 no ip address
 ip nat inside
 ip virtual-reassembly
 duplex auto
 speed auto
!
interface FastEthernet0/0.11
 encapsulation dot1Q 11
 ip address 10.1.1.254 255.255.255.0
!
interface FastEthernet0/0.12
 encapsulation dot1Q 12
 ip address 10.2.2.254 255.255.255.0
!
interface FastEthernet0/0.13
 encapsulation dot1Q 13
 ip address 10.3.3.254 255.255.255.0
!
interface FastEthernet1/0
 ip address dhcp
 ip nat outside
 ip virtual-reassembly
 duplex auto
 speed auto
!
interface FastEthernet2/0
 no ip address
 shutdown
 duplex auto
 speed auto
!
interface FastEthernet3/0
 no ip address
 shutdown
 duplex auto
 speed auto
!
no ip http server
ip forward-protocol nd
!
ip nat inside source list 1 interface FastEthernet0/0 overload
!
access-list 1 permit any
no cdp log mismatch duplex
!
!
!
control-plane
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
end
```

```bash
# Switch 1
Switch#show running-config
Building configuration...

Current configuration : 3712 bytes
!
! Last configuration change at 02:30:26 UTC Sun Oct 24 2021
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname Switch
!
boot-start-marker
boot-end-marker
!
no aaa new-model
!
ip cef
no ipv6 cef
!
spanning-tree mode pvst
spanning-tree extend system-id
!
vlan internal allocation policy ascending
!
interface GigabitEthernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/1
 switchport trunk encapsulation dot1q
 switchport mode trunk
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/2
 switchport trunk encapsulation dot1q
 switchport mode trunk
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/3
 media-type rj45
 negotiation auto
!
ip forward-protocol nd
!
no ip http server
no ip http secure-server
!
[...]
!
end
```

```bash
# Switch 2
Switch#show running-config
Building configuration...

Current configuration : 3789 bytes
!
! Last configuration change at 02:31:46 UTC Sun Oct 24 2021
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname Switch
!
boot-start-marker
boot-end-marker
!
no aaa new-model
!
ip cef
no ipv6 cef
!
spanning-tree mode pvst
spanning-tree extend system-id
!
vlan internal allocation policy ascending
!
interface GigabitEthernet0/0
 switchport access vlan 11
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/1
 switchport access vlan 11
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/2
 switchport access vlan 12
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/3
 switchport access vlan 13
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/0
 switchport trunk allowed vlan 11-13
 switchport trunk encapsulation dot1q
 switchport mode trunk
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/3
 media-type rj45
 negotiation auto
!
ip forward-protocol nd
!
no ip http server
no ip http secure-server
!
[...]
!
end
```

```bash
# Switch 3
Switch#show running-config
Building configuration...

Current configuration : 3726 bytes
!
! Last configuration change at 02:34:52 UTC Sun Oct 24 2021
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname Switch
!
boot-start-marker
boot-end-marker
!
no aaa new-model
!
ip cef
no ipv6 cef
!
spanning-tree mode pvst
spanning-tree extend system-id
!
vlan internal allocation policy ascending
!
interface GigabitEthernet0/0
 switchport trunk encapsulation dot1q
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/1
 switchport access vlan 11
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/2
 switchport access vlan 11
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/3
 switchport access vlan 11
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/0
 switchport access vlan 13
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/3
 media-type rj45
 negotiation auto
!
ip forward-protocol nd
!
no ip http server
no ip http secure-server
!
[...]
!
end
```

### Mettre en place un serveur DHCP dans le nouveau bâtiment